//////////////////////////////////////////////////
//Required
//////////////////////////////////////////////////
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');
var resize = require('gulp-image-resize');
var del = require('del');
//////////////////////////////////////////////////
//Error log
//////////////////////////////////////////////////
function errorlog(error){
	console.log(error.message);
	this.emit('end')
}
//////////////////////////////////////////////////
//Scripts Tasks
//////////////////////////////////////////////////
gulp.task('compress',function(){
	gulp.src('app/image/uncompressed/*')
		.pipe(imagemin({
			progressive:true
		}))
		.pipe(gulp.dest('app/image/compressed'))
		.pipe(reload({stream:true}));
});

var imageInfo={
	0:{
		"imgsize":512,
		"imgname":'icon-512x512'
	},
	1:{
		"imgsize":384,
		"imgname":'chrome-splashscreen-icon-384x384'
	},
	2:{
		"imgsize":192,
		"imgname":'chrome-touch-icon-192x192'
	},
	3:{
		"imgsize":152,
		"imgname":'apple-touch-icon'
	},
	4:{
		"imgsize":144,
		"imgname":'ms-touch-icon-144x144-precomposed'
	},
	5:{
		"imgsize":128,
		"imgname":'icon-128x128'
	},
	6:{
		"imgsize":96,
		"imgname":'icon-96x96'
	},
	7:{
		"imgsize":72,
		"imgname":'icon-72x72'
	}
}
function resizeImg(currentImg,done){
	var size = currentImg.imgsize
	var name = currentImg.imgname
	gulp.src('app/image/icons/base-icon.png')
		.pipe(resize({ 
			imageMagick : true,
			crop : false,
			width : size,
			height : size
		}))
		.pipe(imagemin({
			progressive : true
		}))
		.pipe(rename({
			basename : name
		}))
		.pipe(gulp.dest('app/image/icons/'))
		return done(name);
}
gulp.task('icons',function (){
	for (var index = 0; index < Object.keys(imageInfo).length; index++) {
		resizeImg(imageInfo[index], function(name,err){
			if (!err)
			{
				console.log(name+" done")
			}
			else
			{
				console.log(err)
			}

		});
	};
});

gulp.task('scripts',function(){
	gulp.src(['app/js/uncompressed/**/*.js','!app/js/uncompressed/**/*.min.js'])
		.on('error',errorlog)
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(rename({suffix:'.min'}))
		.pipe(gulp.dest('app/js/compressed'))
		.pipe(reload({stream:true}));
});

gulp.task('styles',function(){
	gulp.src('./app/sass/**/*.scss')
		.on('error',errorlog)
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(rename({suffix:'.min'}))
		.pipe(autoprefixer('last 2 versions'))
		.pipe(gulp.dest('app/css'))
		.pipe(reload({stream:true}));
});
//////////////////////////////////////////////////
//Simple Tasks
//////////////////////////////////////////////////
gulp.task('html',function(){
	gulp.src('app/**/*.html')
		.pipe(reload({stream:true}));
});

gulp.task('manifest',function(){
	gulp.src('app/**/manifest.json')
		.pipe(reload({stream:true}));
});
//////////////////////////////////////////////////
//Build Tasks
//////////////////////////////////////////////////

//remove build folder for when we create a new build
gulp.task('build:clear',function(){
	del([
		'build/**'
	]);
});

//task to create build directory and move out build files over
gulp.task('build:copy',function(){
	return gulp.src('app/**/*/')
		.pipe(gulp.dest('build/'));
});

//remove all unwated files from build aka tidy up the place
gulp.task('build:tidy',function(){
	del([
		'build/sass/',
		'build/image/uncompressed',
		'build/js/uncompressed'
	]);
});

// gulp.task('build',['build:copy','build:tidy']);

//////////////////////////////////////////////////
//Browser-sync Tasks
//////////////////////////////////////////////////
gulp.task('browser-sync',function(){
	browserSync({
		server:{
			baseDir:"./app/"
		}
	});
});

//////////////////////////////////////////////////
//Watch Tasks
//////////////////////////////////////////////////
gulp.task('watch',function(){
	gulp.watch('app/js/uncompressed/**/*.js',['scripts']);
	gulp.watch('app/sass/**/*.scss',['styles']);
	gulp.watch('app/**/*.html',['html']);
	gulp.watch('app/**/manifest.json',['manifest']);
	gulp.watch('app/image/uncompressed/*',['compress']);
});
//////////////////////////////////////////////////
//Default Tasks
//////////////////////////////////////////////////
gulp.task('default',['scripts','compress','styles','html','manifest','browser-sync','watch']);