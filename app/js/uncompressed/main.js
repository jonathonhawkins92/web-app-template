$(function(){

})
//on window load for isotope due to Height:auto on article elements
$(window).load(function(){
	var $main = $('main').isotope({
		// options
		itemSelector: 'article',
		layoutMode: 'masonry',
		percentPosition: true,
		masonry: {
			columnWidth: 'article'
		}
	});
	$main.isotope();
})